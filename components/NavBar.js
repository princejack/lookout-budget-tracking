import { useContext, useEffect, useState } from 'react'
import Navbar from 'react-bootstrap/Navbar'; //acquire the necessary components
import Nav from 'react-bootstrap/Nav';
import Link from 'next/link';
import UserContext from '../UserContext';
import Head from 'next/head'


export default function NavBar(){
  
  const { user } = useContext(UserContext);
  const [getToken, setGetToken] = useState('')

  useEffect(() => {
    const token = localStorage.getItem('token')
    setGetToken(token)
  })
   return (
    <div>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com"/>
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"/>      
      </Head>
    
          <Navbar expand="lg" className="myNavbarBg fixed-top">
          <Link href="/">
             <a className="navbar-brand text-white">Lookout Budget Tracking</a>
          </Link>  
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>  
          <Navbar.Collapse id="navbarCollapse">


               {(getToken !== null)
                 ? (user.isAdmin === true )
                  ? 
                  <>
                     <Link href="/transaction">
                        <a className="nav-link navText ml-auto" role="button">Transactions </a>             
                     </Link>  
                     <Link href="/logout">
                        <a className="nav-link navText" role="button">Logout </a>             
                     </Link>  
                  </> 
                  :
                  <>
                    <Link href="/transaction">
                         <a className="nav-link navText ml-auto"> Transactions </a>             
                     </Link> 
                    <Link href="/logout">
                        <a className="nav-link navText" role="button">Logout </a>             
                     </Link> 
                  </>   
                  :
                  <>
                    <Link href="/login">
                        <a className="nav-link navText ml-auto" role="button">Login</a>
                    </Link>
                    <Link href="/register">
                        <a className="nav-link navText" role="button">Register</a>
                    </Link> 
                  </> 
               }
          
          </Navbar.Collapse>         
          </Navbar>
    </div>      

    )
}