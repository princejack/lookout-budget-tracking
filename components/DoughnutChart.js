import { Doughnut } from 'react-chartjs-2'

export default function DoughtnutChart({income, expenses}) {
	return(
    	<Doughnut data={{
    	    		datasets: [{
    	    			data: [ income, expenses ],
    	    			backgroundColor: [ "#629954", "#780000"  ],
    	
    	    		}],
    	    		labels: [
    	    			'Income',
    	    			'Expenses'
    	    		]
    	    	}} redraw={ false } />
		)
}