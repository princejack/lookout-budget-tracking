import React, { useState, useEffect } from 'react'
import AppHelper from '../app-helper'
import TransactionTable from './TransactionTable'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import numberWithCommas from '../helpers/format'
import Head from 'next/head'
import Swal from 'sweetalert2'
import Router from 'next/router'


export default function Transactions(){

	const [data, setData] = useState([])
	const [userName, setUsersName] = useState([])
	const [targetName, setTargetName] = useState("")




	useEffect(()=> {
		const token = localStorage.getItem('token')
			fetch(`${ AppHelper.API_URL }/users/details`, {
				headers: {'Authorization': `Bearer ${token}`}
			})			
			.then(res => res.json())
			.then(data => {
			 	const passName = data.firstName
			 	setUsersName(passName)
				const passData = data.transactions
				setData(passData)
			})		
	})

		// FUNCTION FOR INCOME/EXPENSE CARDS 
  	const incomeExpensesCard = data.map(transaction => {
  		const amounts = transaction.amount
  		return transaction.amount
  	})
  	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Reduce_of_empty_array_with_no_initial_value
  	const income = incomeExpensesCard.filter(item => item > 0)
  	const incomeTotal = income.reduce((a, b) => a + b, 0)
  	const incomeTotalCard = numberWithCommas(income.reduce((a, b) => a + b, 0).toFixed(2)) 
  	// toFixed returns a string
  	
  	const expenses = incomeExpensesCard.filter(item => item < 0)
  	const expensesTotal = expenses.reduce((a, b) => a + b, 0)
  	// const positiveExpense = (expensesTotal * (-1))
  	const expensesTotalCard = numberWithCommas(expenses.reduce((a, b) => a + b, 0).toFixed(2)) 


  	const balance = numberWithCommas(Math.round((incomeTotal + expensesTotal) * 100) / 100)

	function search(rows){
		return rows.filter((row) => row.name.toLowerCase().indexOf(targetName) > -1)
	}

	const [filterAll, setFilterAll] = useState([])
  	const [transactionTypeselect, setTransctionTypeselect] = useState('')

	function filterFunction(e){
  		e.preventDefault()
  		const filterData = data.filter(transaction => transaction.transactionType.toUpperCase() == transactionTypeselect.toUpperCase())
  		if(filterData){
  			setFilterAll(filterData)
  		} else{
  			setFilterAll([])
  		}
  	}	


	return(
		<>		
		<div className="transactionPageAll transactionPageContainer">

			<div className="searchTableContainer">		
				<Form>
					<Form.Group controlId="searchName" className="transactionSearch">
						<Form.Label>  </Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Search transaction" 
							value={targetName}
							onChange={e => setTargetName(e.target.value)}
							autoComplete="off"
						/>

					</Form.Group>
				</Form>	
				<Form onSubmit={filterFunction}>
					<Form.Group controlId="transactionType" className="transactionfilterType">
						<Form.Control className="dropdown" as="select" value={transactionTypeselect} onChange={e => setTransctionTypeselect(e.target.value)}>
							<option>None</option>
							<option>Income</option>
							<option>Expense</option>
						</Form.Control>
						<Button id="filterBtn" type="submit" className="btn">Filter</Button>
					</Form.Group>
				</Form>						

				<TransactionTable data={search(data)} userName={userName} balance={balance} incomeTotalCard={incomeTotalCard} expensesTotalCard={expensesTotalCard} filterAll={filterAll} />
			</div>
		</div>
		</>			
		)
}