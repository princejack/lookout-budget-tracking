import { Line } from 'react-chartjs-2'

export default function LineChart({income, expenses}) {
	return(
    	<Line data={{
    	    		datasets: [{
    	    			data: [ income, expenses ],
    	    			backgroundColor: [ "green", "red" ],
    	
    	    		}],
    	    		labels: [
    	    			'Income',
    	    			'Expenses'
    	    		]
    	    	}} redraw={ false } />
		)
}