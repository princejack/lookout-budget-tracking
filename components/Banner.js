import Head from 'next/head'
import { Button, Container } from 'react-bootstrap'
import Router from 'next/router'


export default function Banner(){
    function goTransaction(){
      Router.push('/transaction')
    }
	return(
    <div>
      <Head>
        <title>Lookout Budget Tracking</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,600;0,700;1,500&display=swap" rel="stylesheet" />       
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="hero" id="landing-page">
          <Container>
            
           <h2 className="text-left" id="weHelp">we help</h2>
           <h1 className="text-left" >Lookout Budget Tracking</h1>
           <p className="text-left mt-3">Beware of little expenses. A small leak will sink a great ship</p>
           <div className="col text-left">
              <button id="getStarted" className="btn" onClick={goTransaction}>Get Started</button>
           </div>

          </Container>
        </div> 
      </main>
    </div>
		)
}