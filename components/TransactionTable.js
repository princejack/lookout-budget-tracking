import React, {useState, useEffect} from 'react';
import { Container, Row, Col, Table, Button } from 'react-bootstrap'


export default function Datatable({data, userName, balance, incomeTotalCard, expensesTotalCard, filterAll}){
	
	data.sort(function(a,b){
	  return new Date(b.transactionDate) - new Date(a.transactionDate);
	});		
	const transactionRows = data.map(result => {
		return(
			<tr key={result._id}>
					<td>{result.transactionDate.slice(0,10)}</td>
					<td>{result.name}</td>
					<td>{result.category}</td>
					<td>{result.transactionType}</td>
					<td>{result.description}</td>
					<td>&#8369; {result.amount}</td>
					<td>
						<Button variant="warning" className="mr-1">Edit</Button>
						<Button variant="danger">X</Button>
					</td>
				</tr>
			);
	});	


  	const filterTransactionRows = filterAll.map(transaction => {
  		return(
 			<tr key={transaction._id}>
					<td>{transaction.transactionDate.slice(0,10)}</td>
					<td>{transaction.name}</td>
					<td>{transaction.category}</td>
					<td>{transaction.transactionType}</td>
					<td>{transaction.description}</td>
					<td>&#8369; {transaction.amount}</td>
					<td>
						<Button variant="warning" className="mr-1">Edit</Button>
						<Button variant="danger">X</Button>
					</td>
				</tr>
  			)
  	})

	return(
		<div>
			<Container className="transactionBox mb-5">
				<Row><Col xs md>
						<h2 className="text-center">Transactions</h2>
						<Table striped hover className="mt-4">
							<thead>
								<tr>
									<th>Date</th>
									<th>Name</th>
									<th>Category</th>
									<th>Type</th>
									<th>Description</th>
									<th>Amount</th>
									<th>Actions</th>
								</tr>				
							</thead>

								{filterAll.length == 0
										?
										<tbody>{transactionRows}</tbody>									
										:
										<tbody>{filterTransactionRows}</tbody>									
									}
						</Table>
					</Col></Row>
			</Container>
		</div>			
		)
}