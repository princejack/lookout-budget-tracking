module.exports = {
	API_URL: `https://lookout-budget.herokuapp.com/api`,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (response) => response.json()
}