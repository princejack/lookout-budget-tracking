import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'
import Head from 'next/head'	
export default function AddCategory() {

	const [ categoryName, setCategoryName ] = useState('')
	const [ categoryType, setCategoryType ] = useState('')

	useEffect(() => {
  		const token = localStorage.getItem('token')
  		if(token === null) {
  			Swal.fire({
			  icon: 'error',
			  title: 'You must login first'
			})
			Router.push('/login')
  		}
  	}, [])


	function addFunction(e) {
		e.preventDefault()
		let token = localStorage.getItem('token')	
		const options = { 
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({ 
				name: categoryName,
				type: categoryType
			})
		}	

		fetch(`${ AppHelper.API_URL }/users/addCategory`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if(data === true) {
            	Swal.fire({
						  icon: 'success',
						  title: 'Category has been added successfully!'
						})
            	Router.push('/transaction') 
            } else {
            	Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Something went wrong. Please try adding category again.'
				})
            }
        })
	} 

	return(
		<div className="allCategory">
		<Head>
			<title>Add Cateogry</title>
		</Head>
			<Container className="addCategoryContainer">
			  	<Row className="justify-content-center">	
					<Col md={{span:8,offset:2}}>
						<Form onSubmit={addFunction} className="addCategoryForm">
					  		<h2 className="mt-5 mb-5 text-center">Add Category</h2>
						
							{/*Category Type*/}
							<Form.Group controlId="transactionType">
							    <Form.Label className="text-muted">Transaction Type</Form.Label>
							    <Form.Control className="dropdown" as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} autoComplete="off" required >
							      <option value="">Choose Transaction Type:</option>
							      <option>Income</option>
							      <option>Expense</option>
								</Form.Control>
							</Form.Group>
					  		{/*Category Name*/}
					  		<Form.Group className="float-label" controlId="transactionName">
							  	<Form.Label className={categoryName ? "Active" : ""}> Category Name: </Form.Label>
							  	<Form.Control type="text" value={categoryName} placeholder="Category name here" onChange={e => setCategoryName(e.target.value)} autoComplete="off" required />
							</Form.Group>

							{/*Submit Button*/}
							<div>
								<Button id="addCategoriesBtn" type="submit" block>SUBMIT</Button>
							</div>

						</Form>
					
					</Col>
				</Row>
			</Container>
		</div>
		)
}