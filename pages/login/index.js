import React, { useState, useContext, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import View from '../../components/View';
import Router from 'next/router';
import Head from 'next/head';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import styles from '../../styles/Home.module.css'

export default() => {
    return (
            <View title={ 'Login' }>
                <Row className="justify-content-center">
                    <Col xs md="6">
                        
                        <LoginForm />
                    </Col>
                </Row>
            </View>
        )}

    
    const LoginForm = () => {

            const { user, setUser } = useContext(UserContext);
            const [ email, setEmail ] = useState('');
            const [ password, setPassword ] = useState('');
            const [ tokenId, setTokenId ] = useState();

            function authenticate(e) { 
                e.preventDefault();

               
                const options = { 
                    method: 'POST', 
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({email: email, password: password})
                }
                fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
                    if (typeof data.accessToken !== 'undefined') {
                        localStorage.setItem('token', data.accessToken)
                        retrieveUserDetails(data.accessToken)
                    } else {
                        if (data.error === 'does-not-exist') {
                            Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                        } else if (data.error === 'incorrect-password') {
                            Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                        } else if (data.error === 'login-type-error') {
                            Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                        }
                    }
                })
            }

            
            const retrieveUserDetails = (accessToken) => {
                const options = {
                    headers: { Authorization: `Bearer ${accessToken}` } 
                }
                fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
                    setUser({ id: data._id, isAdmin: data.isAdmin })
                    Router.push('/transaction')
                })
            }

            
            const authenticateGoogleToken = (response) => {
                console.log(response) 
                localStorage.setItem('googleToken', response.accessToken)
                const payload = { 
                        method: 'post',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ tokenId: response.tokenId })
                    }

                    fetch (`${ AppHelper.API_URL }/users/verify-google-id-token`, payload)
                    .then(AppHelper.toJSON)
                    .then(data => {
                        if (typeof data.accessToken !== 'undefined'){
                            localStorage.setItem('token', data.accessToken)
                            retrieveUserDetails(data.accessToken)
                            
                        } else { 
                            if (data.error == 'google-auth-error'){ 
                                Swal.fire(
                                    'Google Auth Error',
                                    'Google authentication procedure failed',
                                    'error'
                                )
                            } else if (data.error === 'login-type-error') { // login type error
                                Swal.fire(
                                    'Login Type Error',
                                    'You may have registered through a different login procedure',
                                        'error'
                                )
                            }
                        }
                    })
            }

        return(
            <>
            <div className="avatar"> 
                <img src="/avatar.png" alt="avatar" />
            </div>
            <div className="loginForm">
                    <Form onSubmit={ authenticate }>
                        <Form.Group controlId="userEmail">
                            <Form.Label> Email Address: </Form.Label>
                            <input className="loginInput" type="email" value={email} placeholder="sample@email.com" onChange={(e) => setEmail(e.target.value)} autoComplete="off" required />
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label> Password: </Form.Label>
                            <input className="loginInput" type="password" value={password} placeholder="password123" onChange={(e) => setPassword(e.target.value)} required />
                        </Form.Group>
                        <button className=" btn" type="submit" id="loginBtn" block="true"> Login </button>
                        <GoogleLogin 
                            clientId="1041042020613-5ch3naettdqlck0mge43qnhk3roqb0vp.apps.googleusercontent.com"
                            buttonText="Login using Google"
                            onSuccess={ authenticateGoogleToken }
                            onFailure={ authenticateGoogleToken }
                            cookiePolicy={ 'single_host_origin' } 
                            className="w-100 text-center d-flex justify-content-center"
                        /> 
                    </Form>

            </div>
                                   
                
            </>
            )
    }
    