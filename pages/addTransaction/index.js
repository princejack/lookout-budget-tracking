import { Jumbotron, Form, Button, Row, Col, Container } from 'react-bootstrap'
import React, { useState, useRef, useEffect } from 'react'
import Head from 'next/head'
import AppHelper from '../../app-helper'
import Swal from 'sweetalert2'
import Router from 'next/router'
import Link from 'next/link'

export default function AddTransaction(){
	//hooks
	
	const [ name, setName ] = useState('')
	const [ category, setCategory ] = useState('')
	const [ transactionType, setTransactionType ] = useState('')
	const [ description, setDescription ] = useState('')
	const [ amount, setAmount ] = useState('')

	//transaction category
	const [ transactionCategory, setTransactionCategory ] = useState('')
	// GET CATEGORIES OF USER
	const [ categoriesData, setCategoriesData ] = useState([])
	const isFirstRun = useRef(true);

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setCategoriesData(data.categories)
		})
	}, [])


	useEffect(() => {
		isFirstRun.current  = false;
		return  isFirstRun.current;
			if(categoriesData.length < 1) {
			Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'You do not have categories. Please add one for income/expense.'
				})
			// Router.push('/addCategory')
		}	
	}, [categoriesData])
	


	const categories = categoriesData.map(transaction => transaction)	
	const incomeCategories = categories.filter(category => category.type == "Income")
	const expenseCategories = categories.filter(category => category.type == "Expense")

	const incomeDropdown = incomeCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})
	const expenseDropdown = expenseCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})
	
	useEffect(() => {
		const token = localStorage.getItem('token')

		if(token === null){
			Swal.fire({
				icon: 'error',
				title: 'You must login first!'
			})
			Router.push('/login')
		}
	})

	

	function addTransaction(e){
		e.preventDefault();
		setName('')
		setAmount('')

		const token = localStorage.getItem('token')

		if(transactionType === "Income") {
				const options = { 
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({ 
						name: name,
						category: transactionCategory,
						transactionType: transactionType,
						description: description,
						amount: amount
					})
				}	

				fetch(`${ AppHelper.API_URL }/users/transact`, options)
		        .then(AppHelper.toJSON)
		        .then(data => {
		        	
		            if(data) {
		            	Swal.fire({
								  icon: 'success',
								  title: 'Transaction has been added successfully!'
								})
		            	Router.push('/transaction') 
		            } else {
		            	Swal.fire({
						  icon: 'error',
						  title: 'Oops...',
						  text: 'Something went wrong. Please try again.'
						})
		            }
		        })
			} else {
				const options = { 
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({ 
						name: name,
						category: transactionCategory,
						transactionType: transactionType,
						description: description,
						amount: (amount * -1)
					})
				}	

				fetch(`${ AppHelper.API_URL }/users/transact`, options)
		        .then(AppHelper.toJSON)
		        .then(data => {
		        	
		            if(data ) {
		            	Swal.fire({
								  icon: 'success',
								  title: 'Transaction has been added successfully!',
								  text: 'You may now check this on your transaction history'
								})
		            	Router.push('/transaction') 
		            } else {
		            	Swal.fire({
						  icon: 'error',
						  title: 'Oops...',
						  text: 'Something went wrong. Please try again.'
						})
		            }
		        })
			}
		}

		function gotoAddCategories(){
			Router.push('/addCategory')
		}
	
	
	return(

		<>
		<Head>
			<title>Add Transaction</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,600;0,700;1,500&display=swap" rel="stylesheet" />   
		</Head>
		<div className="addTransactionPageAll">
		<Container className="addTransactionPage">
			
		<Row>
			<Col md={{span:8, offset: 2}}>
				<Form onSubmit={(e) => addTransaction(e)} className="addTransactionForm px-5 py-3 mt-3">
				<h1 className="pb-4 pt-2 text-center">Add Transaction</h1>
					<Form.Group controlId="name" className="float-label">
						<Form.Label>Transaction Name:</Form.Label>
						<input type="text" value={name} onChange={e => setName(e.target.value)} placeholder="Enter transaction name here" autoComplete="off" required />
					</Form.Group>


					<Form.Group controlId="transactionType" className="float-label">
						<Form.Label>Type:</Form.Label>
						<select as="select" 
							value={transactionType}
							onChange={e=>setTransactionType(e.target.value)} 
							required
							className="typeDropdown"
						>				
						<option value=""> transaction type </option> 					
						<option value="Income"> Income </option> 					
						<option value="Expense"> Expense </option> 	
						</select>	
					</Form.Group>

					{/*Transaction Category*/}
							{transactionType == "" 
								?
								<Form.Group controlId="transactionCategory">
									<Form.Label className="text-muted">Transaction Category</Form.Label>
									<div className="d-flex">
									<Form.Control className="dropdown" as="select" value={transactionCategory} 
										onChange={e => setTransactionCategory(e.target.value)} 
										autoComplete="off" required>
										<option>Choose transaction type first</option>
									</Form.Control>
									<a href="/addCategory" className="btn ml-1" id="addCategoryBtn"><div>Add</div></a>
									</div>
								</Form.Group>
								:
								(transactionType == "Income")
									?
									<Form.Group controlId="transactionCategory">
										<Form.Label className="">Transaction Category</Form.Label>
										<div className="d-flex">
											<Form.Control className="dropdown" as="select" value={transactionCategory} 
												onChange={e => setTransactionCategory(e.target.value)} 
												autoComplete="off" required>
												{incomeDropdown}
											</Form.Control>
											 <a href="/addCategory" className="btn ml-1" id="addCategoryBtn"><div>Add</div></a>
										</div>
									</Form.Group>
									:
									<Form.Group controlId="transactionCategory">
										<Form.Label className="">Transaction Category</Form.Label>
										<div className="d-flex">
											<Form.Control className="dropdown" as="select" value={transactionCategory} 
												onChange={e => setTransactionCategory(e.target.value)} 
												autoComplete="off" required>
												{expenseDropdown}
											</Form.Control>
											<a href="/addCategory" className="btn ml-1" id="addCategoryBtn"><div>Add</div></a>
										</div>
									</Form.Group>
							}

				

					<Form.Group controlId="description" className="float-label">
						<Form.Label>Description:</Form.Label>
						<input type="text" value={description} onChange={e => setDescription(e.target.value)} placeholder="Enter Here" autoComplete="off" required />
					</Form.Group>

					<Form.Group controlId="amount" className="float-label">
						<Form.Label>Amount:</Form.Label>
						<input type="number" value={amount} onChange={e => setAmount(e.target.value)} placeholder="Enter Here" autoComplete="off" required />
					</Form.Group>
					
					<button type="submit" className="btn" id="addTransactionBtn">Submit</button>

				</Form>
			</Col>
		</Row>
		</Container>
		</div>
		</>
		)

}