import { useState, useEffect} from 'react'
import Head from 'next/head';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';
import AppHelper from '../../app-helper'


export default function index(){

	const [ formData, setFormData ] = useState({
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		confirmPassword: "",
		mobileNo: ""  
	})
	const { firstName, lastName, email, password, confirmPassword, mobileNo } = formData
	
	const onChangeHandler = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
			
		})
	}

	
	const [isActive , setIsActive] = useState(false)

	useEffect(() => {
	

	}, )

	

	function registerUser(e){
		e.preventDefault();
		
		if((firstName !== '' && lastName !== '' && email !== '' && password !== ''  && confirmPassword !== ''  && mobileNo !== '') && (mobileNo.length === 11) && (confirmPassword===password) ){
			fetch(`${ AppHelper.API_URL }/users/email-exists`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				
				if(data === false){
					fetch(`${ AppHelper.API_URL }/users` , {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify(formData)
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						console.log(data)
						
						if(data=== true){
							Swal.fire({
								icon: 'success',
								title: 'New Account Registered Successfully',
								text: 'You may now Log in'
							})
							Router.push('/login')
						} else{
							Swal.fire({
								icon: "warning",
								title: "Something Went Wrong, Check Credentials",
								text: ""

							})
						}
					})
				}
			})
		}else{
			Swal.fire('Something Went Wrong')
		}
	}


	return (
		<>
			<Head>
				<title>Register Here</title>
			</Head>	

			<Container className="pt-5" id="registerPageContainer">
				<Row>
					<Col md={{span:8, offset: 2}} className="registerForm mt-5">
										
						<Form onSubmit={(e) => registerUser(e)} >
							<Form.Group className="mb-4"> <h3> Register Details </h3></Form.Group>
							<Form.Group>
								<Form.Label>First Name:</Form.Label>
								<input type="text" id="name" name="firstName" className="form-control" value={firstName} placeholder="Insert First Name Here" required autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Last Name:</Form.Label>
								<input type="text" id="lastName" name="lastName" className="form-control" value={lastName} placeholder="Insert Last Name Here" required autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Email Address:</Form.Label>
								<input type="email" id="email" name="email" className="form-control" value={email} placeholder="Insert Email Here" required autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Password:</Form.Label>
								<input type="password" id="password" name="password" className="form-control" value={password} placeholder="Insert Password Here" required autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Confirm Password:</Form.Label>
								<input type="password" id="confirmPassword" name="confirmPassword" className="form-control" value={confirmPassword} placeholder="Confirm Password" required autoComplete="off" onChange={e => onChangeHandler(e)}/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Mobile Number (11 digits):</Form.Label>
								<input type="number" id="mobileNo" name="mobileNo" className="form-control" value={mobileNo} placeholder="(09xx) xxx-xxxx" required autoComplete="off" onChange={e => onChangeHandler(e)}/>

							</Form.Group>

							<button type="submit" className="btn text-white" id="registerBtn">Register</button>
						</Form>
					</Col>
				</Row>
			</Container>
		</>
		)
}