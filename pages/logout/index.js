import { useContext, useEffect } from 'react'
import { Button, Container, Jumbotron } from 'react-bootstrap';
import Router from 'next/router';
import UserContext from '../../UserContext'

export default function index(){

	const { unsetUser } = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		Router.push('/login')
		
	},[])
	return (
		<Container>
			<div className="loaderContainer">
				<div className="loader mx-auto"></div>
				<h1 className="text-center">Logging Out</h1>
			</div>
		</Container>	
		)
}