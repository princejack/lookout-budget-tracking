import React, { useState, useEffect } from 'react'
import AppHelper from '../../app-helper'
import TransactionTable from '../../components/TransactionTable'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import numberWithCommas from '../../helpers/format'
import Head from 'next/head'
import Swal from 'sweetalert2'
import Router from 'next/router'
import AddTransaction from '../../components/AddTransactionComponent'
import Transaction from '../../components/TransactionPage'
import DoughnutChart from '../../components/DoughnutChart';
import LineChart from '../../components/LineChart';


export default function Transactions(){

	const [data, setData] = useState([])
	const [userName, setUsersName] = useState([])
	// const [targetName, setTargetName] = useState("")


	useEffect(() => {
		const token = localStorage.getItem('token')
		if(token === null){
			Swal.fire({
				icon: 'error',
				title: 'Make sure to login first!'
			})
			Router.push('/login')
		}
	},[])

	useEffect(()=> {
		const token = localStorage.getItem('token')
			fetch(`${ AppHelper.API_URL }/users/details`, {
				headers: {'Authorization': `Bearer ${token}`}
			})			
			.then(res => res.json())
			.then(data => {
			 	const passName = data.firstName
			 	setUsersName(passName)
				const passData = data.transactions
				setData(passData)
			})		
	})

		// FUNCTION FOR INCOME/EXPENSE CARDS 
  	const incomeExpensesCard = data.map(transaction => {
  		const amounts = transaction.amount
  		return transaction.amount
  	})
 
  	const income = incomeExpensesCard.filter(item => item > 0)
  	const incomeTotal = income.reduce((a, b) => a + b, 0)
  	const incomeTotalCard = numberWithCommas(income.reduce((a, b) => a + b, 0).toFixed(2)) 
  	
  	const expenses = incomeExpensesCard.filter(item => item < 0)
  	const expensesTotal = expenses.reduce((a, b) => a + b, 0)
  	const expensesTotalCard = numberWithCommas(expenses.reduce((a, b) => a + b, 0).toFixed(2)) 
  	const finalExpense = expensesTotal * -1


  	const balance = numberWithCommas(Math.round((incomeTotal + expensesTotal) * 100) / 100)




	return(
		<>			
			<Head>
				<title>Transactions Page</title>
				<link rel="preconnect" href="https://fonts.gstatic.com"/>
				<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet"/>			
			</Head>
			<Container fluid>
				<Row className="actualTransactionPage">
					<Col md={5} className="addTransactionComponent">
						<h2 className="text-center">Welcome, {userName}!</h2>
						<h2 className="text-center"> Balance:  &#8369;{balance}</h2>
						<Row>
							<div className="balanceContainer">
								<h3 className="incomeText text-center">Total Income: &#8369;{incomeTotalCard}</h3>
								<h3 className="expenseText text-center">Total Expense: &#8369;{expensesTotalCard}</h3>
							</div> 
						</Row>						
						<AddTransaction />
					</Col>
					<Col md={7}>
						<Transaction />
					</Col>
				</Row>
				<Row>				
					<Col  md={6} className="chartContainer pb-3">
					<div className="deets">
						<h3 className="text-center breakdownDetails">Total Income: &#8369;{incomeTotal}</h3>
						<h3 className="text-center breakdownDetails">Total Expenses: &#8369;{finalExpense}</h3>
						<h3 className="text-center breakdownDetails">Balance: &#8369;{balance}</h3>
					</div>
						<div className="mt-5">
							<DoughnutChart income={incomeTotal} expenses={expensesTotal} />
						</div>
					</Col>			
					<Col  md={6} className="chartContainer pb-3">
						<Container className="mt-5">
							<LineChart income={incomeTotal} expenses={expensesTotal} />
						</Container>
					</Col>
				</Row>					
			</Container>
		</>			
		)
}