import { Container } from 'react-bootstrap';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { UserProvider } from '../UserContext' //lets import our context provider 
import { useState, useEffect } from 'react'
import Navbar from '../components/NavBar'
import Swal from 'sweetalert2'

export default function MyApp({ Component, pageProps }) {
	//state hook for the user state here for global scoping 
	const [ user, setUser] = useState({
		// value is initialized as an object with for the user with the properties set to null
		// the proper values will be obtained from the local storage AFTER component gets rendered due to NextJS pre rendering
		email: null,
		isAdmin: null
	})
	
	//localStorage can only be accessed after this component has been rendered, hence the "need" for an effect hook
	useEffect(() => {
		setUser({
			email: localStorage.getItem('email'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])

	
	//lets create an effect hook for testing the setUser() functionality
	useEffect(() => {
		console.log(user.email);
	}, [user.email])
  
	//lets create a function for our logout component that will clear out the data from our local storage.
	const unsetUser = () => {
		localStorage.clear()
		//lets reset the user global scope in the context provider to have its email and isAdmin props revert back to its initial value/ state
		setUser({
			email: null
		});
	}
  return(
  	<>
	{/*wrap the component tree within the UserProvider context provider so that the components will have access to the passed values in this module.*/}

	<UserProvider value={{user, setUser, unsetUser}}>
  		<Navbar />
	  	<div>
	  		<Component {...pageProps} />
	  	</div>
	</UserProvider>
  	</>
  	) 
}