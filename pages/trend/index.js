import { useState, useEffect } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import LineChart from '../../components/LineChart';
import Head from 'next/head';
import AppHelper from '../../app-helper';


export default function Home(data){
	const [userDetails, SetUserDetails] = useState([])
	
	useEffect(() => {
		const token = localStorage.getItem('token')
		if(token === null){
			Swal.fire({
				icon: 'error',
				title: 'Make sure to login first!'
			})
			Router.push('/login')
		} else{
			
			fetch(`${ AppHelper.API_URL }/users/details`, {
				headers: {'Authorization': `Bearer ${token}`}
			})			
			.then(res => res.json())
			.then(data => {
			 
				const passData = data.transactions
				SetUserDetails(passData)
			})				
		}

	},[])

	// FUNCTION FOR INCOME/EXPENSE CARDS 
  	const incomeExpensesCard = userDetails.map(transaction => {
  		const amounts = transaction.amount
  		return transaction.amount
  	})
  	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Reduce_of_empty_array_with_no_initial_value
  	const income = incomeExpensesCard.filter(item => item > 0)
  	const incomeTotal = income.reduce((a, b) => a + b, 0)

  	// toFixed returns a string
  	
  	const expenses = incomeExpensesCard.filter(item => item < 0)
  	const expensesTotal = expenses.reduce((a, b) => a + b, 0)

 

  	const balance = Math.round((incomeTotal + expensesTotal) * 100) / 100





  	return(
  		<>
  		<Head>
  			<title>Expenses Breakdown</title>
  		</Head>
	
				
		<Container fluid>
			<Row>
					

				<Col  md={{span: 8, offset: 2}} className="chartContainer pb-3">
					<div className="mt-5">
						<LineChart income={incomeTotal} expenses={expensesTotal} />
						
					</div>
				</Col>
			</Row>	
		</Container>

		
  		</>
  		)

}